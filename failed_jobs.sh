#~/bin/bash

echo "PROJECT_ID: $2"
echo "TOKEN: $1"
echo: "STAGE: $3"
curl --globoff --header "PRIVATE-TOKEN: $1" "https://gitlab.com/api/v4/projects/$2/jobs" | jq '[.[] | select((.name == "'$3'") and (.status == "failed"))] | .[0] | {job_id: .id, job_name: .name, job_status: .status}'

